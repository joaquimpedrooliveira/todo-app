# Todo App
API em NodeJS para gerenciar tarefas.

## Pré-requisitos
- NodeJS = 10.4.x
- [Nodemon](https://nodemon.io/)
- MongoDB 5.0.x executando na porta default (`27017`)

## Comandos
### Instalar as dependências
`npm install`

### Executar a aplicação para desenvolvimento (hot deploy)
`npm dev`

### Executar a aplicação no servidor
`npm start`

### Executar os testes unitários
`npm test`

### Executar o mongoDB via docker
`docker run --name mongodb -p 27017:27017 mongo:5`


### Executar o Postman via docker
Nota: `--add-host` adiciona o IP do host no DNS do contêiner

```shell
cd postman-tests
HOST_IP=<ip do host>
docker run -v $PWD:/etc/newman \
--add-host=server:${HOST_IP} \
-t postman/newman:alpine \
run TodoApp.postman_collection.json \
--environment="TodoApp-Docker.postman_environment.json"
```
